import Foundation
import SwiftSoup

public final class Crawler {
  private let arguments: [String]

  public init(arguments: [String] = CommandLine.arguments) {
    self.arguments = arguments
  }

  public func run() throws {
    print("Hello world! Now we will parse some HTML")

    do {
      let html = "<html><head><title>First parse</title></head>"
        + "<body><p>Parsed HTML into a doc.</p></body></html>"
      let doc: Document = try SwiftSoup.parse(html)
      print(try doc.text())
    } catch Exception.Error(let type, let message) {
      print(message)
    } catch {
      print("error")
    }
  }
}
