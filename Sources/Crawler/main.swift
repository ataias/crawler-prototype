import CrawlerCore

let tool = Crawler()

do {
  try tool.run()
} catch {
  print("Whoops! An error occurred: \(error)")
}
